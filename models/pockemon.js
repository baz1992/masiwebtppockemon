var mongoose = require("mongoose");
var Schema = mongoose.Schema;
mongoose.Promise = global.Promise;
//require('mongoose').model('User');
var db =mongoose.createConnection('mongodb://baz:baz1234@ds149437.mlab.com:49437/masiwebtp1');


var UserSchema = mongoose.Schema({
    email: String,
	password: String,
	teamPockemons : [{ type: Schema.Types.ObjectId, ref: 'Pockemon' }]
});



module.exports = db.model('User', UserSchema)

// create a schema
var PockemonSchema = mongoose.Schema({
    name: String,
	urlPhoto: String,
	speed: Number,
	specialDefense: Number,
	specialAttack: Number,
	defense: Number,
	attack: Number,
	hp: Number,
	attacks: [{ type: String }],
	object: String,
	no_damage_to: String,
	half_damage_to: String,
	double_damage_to: String,
	no_damage_from: String,
	half_damage_from: String,
	double_damage_from: String,
	user : { type: Schema.Types.ObjectId, ref: 'User' }
});

/*
PockemonSchema.pre('remove', function(next) {

    this.model('User').update(
        { _id: this.user._id }, 
        { $pull: { teamPockemons: this._id } }, 
        { multi: true },
        next);
});
*/
module.exports = db.model('Pockemon', PockemonSchema)