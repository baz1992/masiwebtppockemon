var mongoose = require("mongoose");
var Schema = mongoose.Schema


mongoose.Promise = global.Promise;
var db =mongoose.createConnection('mongodb://baz:baz1234@ds149437.mlab.com:49437/masiwebtp1');

var PockemonSchema = mongoose.Schema({
    name: String,
	urlPhoto: String,
	speed: Number,
	specialDefense: Number,
	specialAttack: Number,
	defense: Number,
	attack: Number,
	hp: Number,
	attacks: [{ type: String }],
	object: String,
	no_damage_to: [{ type: String }],
	half_damage_to: [{ type: String }],
	double_damage_to: [{ type: String }],
	no_damage_from: [{ type: String }],
	half_damage_from: [{ type: String }],
	double_damage_from: [{ type: String }],
	user : { type: Schema.Types.ObjectId, ref: 'User' }
});



module.exports = db.model('Pockemon', PockemonSchema)

// create a schema
var UserSchema = mongoose.Schema({
    email: String,
	password: String,
	teamPockemons : [{ type: Schema.Types.ObjectId, ref: 'Pockemon' }]
});



module.exports = db.model('User', UserSchema)

