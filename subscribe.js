const subscribe = require('express').Router();
var path    = require("path");
var mongoose = require('mongoose');
var bodyParser = require('body-parser');


var Schema = mongoose.Schema;

subscribe.get('/', (req, res) => {

    res.sendFile(path.join(__dirname+'/views/subscribe.html'));
});

subscribe.post('/', (req, res) => {
	
	var mail= req.body.email;
	var passwordParam= req.body.password;
	 
	 var UserPost = require(__dirname+"/models/user");
	 
	 UserPost.findOne({ 'email': mail },  function (err, user) {
		if (err) return handleError(err);
		
			if ( user == null)
			{
				var newUser = new UserPost({email: mail, password: passwordParam});
				newUser.save(function (err, userInsert) {
					if (err) {
						return err;
					}
					else {
						req.session.mail = mail;
						req.session.idUser =userInsert._id;
							res.redirect("/home");
							return;						
						}
				});
			}
			else
			{
				res.send('user already exist');
			}

		})
});

//ajax http://localhost:8080/subscribe/loginExist
subscribe.post('/loginExist', (req, res) => {
	
	//console.log('body: ' + req.body.email);

	var mail= req.body.email;
	 
	 var UserPost = require(__dirname+"/models/user");
	 
	 UserPost.findOne({ 'email': mail },  function (err, user) {
		if (err) return handleError(err);
		
			if ( user == null)
			{
				 res.contentType('json');
				 res.send({ userExist: 'false'});
			}
			else if(user.email == mail )
			{
				 res.contentType('json');
				 res.send({ userExist: 'true'});
			}

	});
	
});

module.exports = subscribe;