const home = require('express').Router();
var path    = require("path");

var idUser ='5826673199a1ed01a8220972';
var userLog ='basil.timmermans2333@gmail.com';

home.get('/', (req, res) => {

		//console.log(req.session.mail);
			if(req.session.mail == null)
			{
				 res.redirect("/login");
				 return;
			}

    res.render('home',{user: req.session.mail});
});

home.post('/addPockemon', (req, res) => {
			
			//console.log(req.body);

			var PockemonPost = require(__dirname+"/models/pockemon");
			
			
			
			var pockemon = new PockemonPost({name:  req.body.name, urlPhoto: req.body.urlImage, speed: req.body.dictStat[0].value, specialDefense: req.body.dictStat[1].value,
			specialAttack: req.body.dictStat[2].value, defense: req.body.dictStat[3].value, attack: req.body.dictStat[4].value,
			hp: req.body.dictStat[5].value ,user: req.session.idUser , no_damage_to: req.body.no_damage_to, half_damage_to: req.body.half_damage_to, double_damage_to: req.body.double_damage_to,
			no_damage_from: req.body.no_damage_from, half_damage_from: req.body.half_damage_from, double_damage_from: req.body.double_damage_from
			});
			
			var monPockemon = null;
			pockemon.save(function (err, pockemon) {
					if (err) {
						return err;
					}
					else
					{
						//console.log(pockemon);
						monPockemon = pockemon;
						
						PockemonPost.findOne({'_id' : monPockemon._id  }, function (err, pockemonToFind) {
						if (err) return handleError(err);
						
							monPockemon=pockemonToFind;
						
							
						var UserPost = require(__dirname+"/models/user");
				 
										 UserPost.findOne({ 'email': req.session.mail },  function (err, user) {
											if (err) return handleError(err);
												user.teamPockemons.push(pockemon);
												
												user.save( function (err, user) {
												if (err) return handleError(err);
													
													 res.contentType('json');
													res.send(monPockemon);
												});
											});
					
					});
				}
				});
			

			

});

home.get('/getpockemonUser', (req, res) => {

	
	var PockemonGET = require(__dirname+"/models/pockemon");
	var UserGET = require(__dirname+"/models/user");
	
				UserGET.findOne({ 'email': req.session.mail }).populate('teamPockemons') 
		.exec(function (err, person) {
		  if (err) return handleError(err);
		  //console.log(person);
		  
		  res.contentType('json');
			res.send(person.teamPockemons);
		});
			
});

home.post('/addPockemonAttack', (req, res) => {

	var PockemonGET = require(__dirname+"/models/pockemon");
	var idPockemon= req.body.idPockemon;
	
	
	
	PockemonGET.findOne({ 'user': req.session.idUser, '_id' : idPockemon  }, function (err, pockemon) {
			if (err) return handleError(err);
		
									pockemon.attacks.push(req.body.name);
									
									pockemon.save( function (err, user) {
									if (err) return handleError(err);
										
										  res.contentType('json');
										res.send({ status: 'done'});
									});

	})

});


home.post('/deletePockemonAttack', (req, res) => {

	var PockemonGET = require(__dirname+"/models/pockemon");
	var idPockemon= req.body.idPockemon;

	//console.log(req.body);
	
	PockemonGET.findOne({ 'user': req.session.idUser, '_id' : idPockemon  }, function (err, pockemon) {
			if (err) return handleError(err);
		
									pockemon.attacks.pull(req.body.name);
									
									pockemon.save( function (err, user) {
									if (err) return handleError(err);
										
										  res.contentType('json');
										res.send({ status: 'done'});
									});

	})

});

home.post('/addPockemonObject', (req, res) => {

	var PockemonGET = require(__dirname+"/models/pockemon");
	var idPockemon= req.body.idPockemon;

	
	PockemonGET.findOne({ 'user': req.session.idUser, '_id' : idPockemon  }, function (err, pockemon) {
			if (err) return handleError(err);
		
									pockemon.object=req.body.name;
									
									pockemon.save( function (err, user) {
									if (err) return handleError(err);
										
										  res.contentType('json');
										res.send({ status: 'done'});
									});

	})

});

home.post('/deletePockemonObject', (req, res) => {

	var PockemonGET = require(__dirname+"/models/pockemon");
	var idPockemon= req.body.idPockemon;

	
	PockemonGET.findOne({ 'user': req.session.idUser, '_id' : idPockemon  }, function (err, pockemon) {
			if (err) return handleError(err);
		
									pockemon.object="";
									
									pockemon.save( function (err, user) {
									if (err) return handleError(err);
										
										  res.contentType('json');
										res.send({ status: 'done'});
									});

	})

});

home.post('/deletePockemon', (req, res) => {

	var PockemonGET = require(__dirname+"/models/pockemon");
	var idPockemon= req.body.idPockemon;

	//console.log(idPockemon);
	PockemonGET.findOne({ 'user': req.session.idUser, '_id' : idPockemon  }, function (err, pockemon) {
			if (err) return handleError(err);
		
																		
									pockemon.remove( function (err, user) {
									if (err) return handleError(err);
										
										  res.contentType('json');
										res.send({ status: 'done'});
									});

	})
	
	
});

home.get('/logout', (req, res) => {

			req.session.mail =null;
			req.session.idUser = null;
			
				 res.redirect("/login");
				 return;
	
});
	


module.exports = home;


/*

	PockemonGET.findOne({ 'user': idUser, '_id' : idPockemon  }, function (err, pockemon) {
			if (err) return handleError(err);
		
									pockemon.object="";
									
									pockemon.remove( function (err, user) {
									if (err) return handleError(err);
										
										  res.contentType('json');
										res.send({ status: 'done'});
									});

	})

	
	
	    PockemonGET.findById(req.params.id, function(err, pockemon){
			pockemon.remove(function(err){
             if(!err) {
                 UserGET.update({_id: idUser}, 
                      {$pull: {teamPockemons: pockemon._id}}, 
                          function (err, numberAffected) {
                            console.log(numberAffected);
							}
						);
						}
                       else {
                        console.log(err);                                      
                    }
                  
				 
            });
        });
*/


