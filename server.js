
var mongoose = require('mongoose');
var session = require('express-session');


var express = require('express');
var bodyParser = require('body-parser');

const login = require('./login');
const subscribe = require('./subscribe');
const home = require('./home');


var app = express();


app.set('view engine', 'ejs');

app.use(session({
    secret: 'jazyBaz',
    resave: true,
    saveUninitialized: true
}));

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use('/styles',express.static(__dirname + '/public')); // http://localhost:8080/styles/signin.css
app.use('/login', login);
app.use('/subscribe', subscribe);
app.use('/home', home);

app.listen(8080);